package main

import "fmt"

// robotDirectionChanges maps a direction to a left and right rotation in that order
// e.g. robotDirectionChanges["NORTH"][0] is a left rotation from NORTH.
var robotDirectionChanges map[string][]string = map[string][]string{
	"NORTH": []string{"WEST", "EAST"},
	"EAST":  []string{"NORTH", "SOUTH"},
	"SOUTH": []string{"EAST", "WEST"},
	"WEST":  []string{"SOUTH", "NORTH"},
}

// robot is the robot being controlled, i.e. the actor
type robot struct {
	direction   string
	containedIn *unit
	controller  *controller
}

func (r *robot) rotateLeft() (string, error) {
	if r.containedIn == nil {
		return "", fmt.Errorf("cannot rotate unplaced robot")
	}
	r.direction = robotDirectionChanges[r.direction][0]
	return r.direction, nil
}

func (r *robot) rotateRight() (string, error) {
	if r.containedIn == nil {
		return "", fmt.Errorf("cannot rotate unplaced robot")
	}
	r.direction = robotDirectionChanges[r.direction][1]
	return r.direction, nil
}

func (r robot) isPlaced() bool {
	return r.containedIn != nil
}
