/*

Command toyrobotsimulator simulates a robot walking on a tabletop grid.

Usage

Run application and serve commands from STDIN

	$ go build && ./toyrobotsimulator
	PLACE 0,0,NORTH
	REPORT
	0,0,NORTH


Run application and serve commands from file.

	$ go build && cat commands.txt | ./toyrobotsimulator
	0,0,NORTH


Run application with debugging visualisation

	$ go build && ./toyrobotsimulator -enable_grid_visualisation
	PLACE 2,3,WEST
	---------------
	[ ][ ][ ][ ][ ]
	[ ][ ][<][ ][ ]
	[ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ]
	---------------

Run application with visualisation & custom dimensions

	$ go build && ./toyrobotsimulator -enable_grid_visualisation -X=7 -Y=7
	PLACE 6,6,EAST
	---------------------
	[ ][ ][ ][ ][ ][ ][>]
	[ ][ ][ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ][ ][ ]
	[ ][ ][ ][ ][ ][ ][ ]

*/
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

var xCells = 5
var yCells = 5
var visualisation bool

func init() {
	flag.IntVar(&xCells, "X", 5, "number of cells on X axis")
	flag.IntVar(&yCells, "Y", 5, "number of cells on Y axis")
	flag.BoolVar(&visualisation, "enable_grid_visualisation", false, "prints grid visualisation of board on every update")
	flag.Parse()
}

func main() {
	board, robot := newGame(os.Stdin, os.Stdout)
	runGame(board, robot)
}

// NewGame creates a game with a given input and output
func newGame(input io.Reader, output io.Writer) (*board, *robot) {
	board := newBoard(xCells, yCells, output)
	robot := robot{
		controller: newController(input),
	}
	return &board, &robot
}

// runGame waits for a robots controller input, then delegates any commands appropriate handler function
func runGame(board *board, robot *robot) {
	for input := range robot.controller.inputs {
		if input.code == Place {
			handlePlaceInput(board, robot, input)
		} else if input.code == Move {
			handleMoveInput(board, robot, input)
		} else if input.code == Left {
			handleLeftInput(board, robot, input)
		} else if input.code == Right {
			handleRightInput(board, robot, input)
		} else if input.code == Report {
			handleReportInput(board, robot, input)
		}
	}
}

// handlePlaceInput processes a single place command and applies it to board/robot
func handlePlaceInput(board *board, robot *robot, input inputInstance) {
	var x, y int64
	var err error
	if x, err = strconv.ParseInt(string(input.args[0]), 10, 64); err != nil {
		log.Printf("failed parsing X from input '%s'", input.args[0])
		return
	}
	if y, err = strconv.ParseInt(string(input.args[1]), 10, 64); err != nil {
		log.Printf("failed parsing y from input '%s'", input.args[1])
		return
	}
	if err := board.place(robot, coordinate{int(x), int(y)}, string(input.args[2])); err != nil {
		log.Print(err)
	} else {
		if visualisation {
			board.display()
		}
	}
}

// handleMoveInput processes a single move command and applies it to board/robot
func handleMoveInput(board *board, robot *robot, input inputInstance) {
	if err := board.move(robot); err != nil {
		log.Print(err)
	} else if visualisation {
		board.display()
	}
}

// handleLeftInput processes a single left command and applies it to board/robot
func handleLeftInput(board *board, robot *robot, input inputInstance) {
	robot.rotateLeft()
	if visualisation {
		board.display()
	}
}

// handleRightInput processes a single right command and applies it to board/robot
func handleRightInput(board *board, robot *robot, input inputInstance) {
	robot.rotateRight()
	if visualisation {
		board.display()
	}
}

// handleReportInput processes a single report command and applies it to board/robot
func handleReportInput(board *board, robot *robot, input inputInstance) {
	if visualisation {
		board.display()
	}
	if robot.containedIn != nil {
		fmt.Fprintf(board.output, "%d,%d,%s\n", robot.containedIn.coord.x, robot.containedIn.coord.y, robot.direction)
	}
}
