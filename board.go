package main

import (
	"fmt"
	"io"
	"strings"
)

// directions maps possible cardinal directions to a display icon
var directions map[string]string = map[string]string{
	"NORTH": "^",
	"EAST":  ">",
	"SOUTH": "v",
	"WEST":  "<",
}

// coordinate is a coordinate in (X,Y) to our board.
type coordinate struct {
	x int
	y int
}

func (c coordinate) String() string {
	return fmt.Sprintf("(X=%d, Y=%d)", c.x, c.y)
}

// unit is a single slot, pointed to by a single coordinate, and able to contain a single robot.
type unit struct {
	contains *robot
	coord    coordinate
}

// board is the playable area, represented as a cartesian plane in a multidimensional array.
type board struct {
	// plane maps coordinates to a unit, and is layed out to be indexed directly by a coordinate
	plane [][]unit
	// convenience to know the shape of board.
	xLen int
	yLen int

	// any output produced by commands will be written here
	output io.Writer
}

// newBoard creates a board of a given shape
// the entire plane is preallocated, and ready to be used
func newBoard(xSlots, ySlots int, output io.Writer) board {
	plane := make([][]unit, xSlots)
	for xi, _ := range plane {
		plane[xi] = make([]unit, ySlots)

		for yi, _ := range plane[xi] {
			plane[xi][yi].coord = coordinate{xi, yi}
		}
	}
	return board{
		plane:  plane,
		xLen:   xSlots,
		yLen:   ySlots,
		output: output,
	}
}

// place attempts to put a robot at a set of coordinates facing a given direction.
// it will fail to do so if the cooordinates or direction are illegal.
// this method also maintains bookkeeping in the robot about where (if at all)
// it is placed on the board.
func (b *board) place(robot *robot, coord coordinate, direction string) error {
	if !b.validateCoords(coord) {
		return fmt.Errorf("tried to move to %s, is not legal within board of (X:%d, Y:%d)", coord, b.xLen-1, b.yLen-1)
	}

	// face robot the right way
	if _, ok := directions[direction]; !ok {
		return fmt.Errorf("cannot face robot in direction '%s', legal directions are %#v", direction, directions)
	}
	robot.direction = direction

	// now that we're sure the place() is valid, remove robot from its current unit
	if robot.containedIn != nil {
		robot.containedIn.contains = nil
	}

	// place robot, and maintain two way bookkeeping.
	// a unit knows it contains a certain robot.
	// a robot knows it is contained in a certain unit.
	b.plane[coord.x][coord.y].contains = robot
	robot.containedIn = &b.plane[coord.x][coord.y]
	return nil
}

// validateCoords checks if desired coordinates are legal on this board.
func (b board) validateCoords(coord coordinate) bool {
	return (coord.x >= 0 && coord.x < b.xLen) && (coord.y >= 0 && coord.y < b.yLen)
}

// display visualises the board as quadrant 1 of a cartesian plane
func (b board) display() {
	// verical separator along x axis, needs 3 "-"s per slot to match printed output below.
	separator := strings.Repeat("-", b.xLen*3)
	fmt.Println(separator)

	// to accomplish this in a print statement, we need to iterate
	// over our cells in a along the y and then x axis.
	for y := b.yLen - 1; y >= 0; y-- {
		for x := 0; x < b.xLen; x++ {
			// we'll represent a cell with an icon
			// it can be empty, or a robot facing in the
			// correct direction
			icon := " "
			occupant := b.plane[x][y].contains
			if occupant != nil {
				icon = directions[occupant.direction]
			}
			fmt.Printf("[%s]", icon)
		}
		fmt.Printf("\n")
	}
	fmt.Println(separator)
}

// move moves the robot one space in its current direction
// this method merely orchestrates where that new position
// will be, all action is delegated to place().
func (b *board) move(r *robot) error {
	if !r.isPlaced() {
		return fmt.Errorf("cannot move robot before initial placement")
	}

	// calculate new coordinates given current robot direction
	// our board layout defines north as a y++, and east as a x++
	newCoordinates := r.containedIn.coord
	if r.direction == "NORTH" {
		newCoordinates.y++
	} else if r.direction == "EAST" {
		newCoordinates.x++
	} else if r.direction == "SOUTH" {
		newCoordinates.y--
	} else if r.direction == "WEST" {
		newCoordinates.x--
	}

	// see if our new coordinates are valid on this board.
	if !b.validateCoords(newCoordinates) {
		return fmt.Errorf("tried to move to illegal coordinate %s", newCoordinates)
	}

	// place in new coordinates, facing same direction
	b.place(r, newCoordinates, r.direction)

	return nil
}
