package main

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"testing"
	"time"
)

var reportMatcher *regexp.Regexp = regexp.MustCompile(`(\d+),(\d+),(\w+)`)

// testCase contain a list of input strings (exactly as presented, so embed necessary newlines)
// and the expected output of a REPORT after all input is processed.
type testCase struct {
	commands     []string
	reportOutput string
}

var testCases []testCase = []testCase{
	{
		[]string{"PLACE 0,0,NORTH\n"},
		"0,0,NORTH",
	},
	{
		[]string{
			"PLACE 0,0,NORTH\n",
			"MOVE\n",
		},
		"0,1,NORTH",
	},
	{
		[]string{
			"PLACE 0,0,NORTH\n",
			"LEFT\n",
		},
		"0,0,WEST",
	},
	{
		[]string{
			"PLACE 1,2,EAST\n",
			"MOVE\n",
			"MOVE\n",
			"LEFT\n",
			"MOVE\n",
		},
		"3,3,NORTH",
	},
}

func TestBadPlace(t *testing.T) {
	in_reader, in_writer := io.Pipe()
	_, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	// start game, then close in_writer causing game to shutdown when we're done
	go runGame(board, robot)
	defer in_writer.Close()

	// place robot in an illegal coord
	writeOrFatal(t, in_writer, fmt.Sprintf("PLACE %d,%d,SOUTH\n", board.xLen, board.yLen))

	// place robot in a really illegal coord
	writeOrFatal(t, in_writer, fmt.Sprintf("PLACE %d,%d,SOUTH\n", 1<<32, -1*1<<32))

	// cant assert its in the right place, and hard to assert absense of a REPORT
	// we'll just be happy this didn't crash.
}

// TestWalkOffEdge will try to walk to and off corners
// this will validate that we prevent the robot falling, and
// also that we can move again after hitting this condition
func TestWalkOffCorner(t *testing.T) {
	in_reader, in_writer := io.Pipe()
	out_reader, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	// start game, then close in_writer causing game to shutdown when we're done
	go runGame(board, robot)
	defer in_writer.Close()

	// walks N steps in current direction
	walkN := func(n int) {
		for i := 0; i < n; i++ {
			writeOrFatal(t, in_writer, "MOVE\n")
		}
	}

	// start at 0,0 facing south
	writeOrFatal(t, in_writer, "PLACE 0,0,SOUTH\n")
	// try to walk off 0,0 corner
	walkN(board.yLen * 2)
	// we shold still be at 0,0
	if err := assertPosition(t, in_writer, out_reader, 0, 0, ""); err != nil {
		t.Fatalf("tried to walk off SOUTH WEST corner, but current state is not still in corner: %s", err)
	}
	// walk off NORTH WEST corner
	writeOrFatal(t, in_writer, "LEFT\nLEFT\n")
	walkN(board.yLen * 2)
	// should be at 0,MAXY
	if err := assertPosition(t, in_writer, out_reader, 0, board.yLen-1, ""); err != nil {
		t.Fatalf("tried to walk off NORTH WEST corner, but current state is not still in corner: %s", err)
	}
}

// TestCommandSequences performs some example sequences of commands
// from the spec, and validates they land in the correct position.
func TestCommandSequences(t *testing.T) {
	for i, testCase := range testCases {
		in_reader, in_writer := io.Pipe()
		out_reader, out_writer := io.Pipe()
		board, robot := newGame(in_reader, out_writer)

		// start game, then close in_writer causing game to shutdown when we're done
		go runGame(board, robot)
		defer in_writer.Close()

		// write testCase commands to input
		for _, cmd := range testCase.commands {
			writeOrFatal(t, in_writer, cmd)
		}
		// call a REPORT to see where we are
		writeOrFatal(t, in_writer, "REPORT\n")

		// wait for output
		read, err := blockingReadLine(out_reader)
		if err != nil {
			t.Fatalf("could not read output: %s", err)
		}

		if !reportMatcher.Match(read) {
			t.Fatalf("[%d] expected to find REPORT results command sequence %#v, instead got %s", i, testCase.commands, read)
		}

		// validate output matches out expected output
		if string(read) != testCase.reportOutput {
			t.Fatalf("[%d] expected command sequence %#v to produce REPORT output of %s, got %s", i, testCase.commands, testCase.reportOutput, read)
		}
	}
}

// TestPlaceDirect applies a place command directly (i.e. bypasses controller)
func TestPlaceDirect(t *testing.T) {
	in_reader, _ := io.Pipe()
	_, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	// inject a command in directly
	args := [][]byte{
		[]byte{'2'}, []byte{'3'}, []byte("WEST"),
	}
	placeInput := inputInstance{
		code: Place,
		args: args,
	}
	handlePlaceInput(board, robot, placeInput)

	// peek at robot internals and check they're at the right spot
	if robot.containedIn == nil {
		t.Fatalf("manually placed robot, but robot has no robot.containedIn ptr")
	}

	if robot.containedIn.coord.x != 2 || robot.containedIn.coord.y != 3 {
		t.Fatalf("manually placed robot at 2,3,WEST, but robot returns: %#v", robot.containedIn.coord)
	}
}

// TestIgnoreBeforePlace confirms that commands preceding a PLACE are safely ignore
func TestIgnoreBeforePlace(t *testing.T) {
	in_reader, in_writer := io.Pipe()
	out_reader, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	go runGame(board, robot)
	defer in_writer.Close()

	for _, cmd := range []string{"MOVE\n", "LEFT\n", "RIGHT\n", "REPORT\n"} {
		writeOrFatal(t, in_writer, cmd)
	}
	writeOrFatal(t, in_writer, "PLACE 0,0,NORTH\n")
	if err := assertPosition(t, in_writer, out_reader, 0, 0, "NORTH"); err != nil {
		t.Fatalf("expected to be as placed in 0,0,NORTH: %s", err)
	}
}

// TestRotateLeft confirms a LEFT faces us in the expected direction
func TestRotateLeft(t *testing.T) {
	in_reader, in_writer := io.Pipe()
	out_reader, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	go runGame(board, robot)
	defer in_writer.Close()

	writeOrFatal(t, in_writer, "PLACE 0,0,NORTH\n")

	for _, expected := range []string{"WEST", "SOUTH", "EAST"} {
		writeOrFatal(t, in_writer, "LEFT\n")
		if err := assertPosition(t, in_writer, out_reader, 0, 0, expected); err != nil {
			t.Fatalf("tried to rotate LEFT: %s", err)
		}
	}
}

// TestRotateRight confirms a RIGHT faces us in the expected direction
func TestRotateRight(t *testing.T) {
	in_reader, in_writer := io.Pipe()
	out_reader, out_writer := io.Pipe()
	board, robot := newGame(in_reader, out_writer)

	go runGame(board, robot)
	defer in_writer.Close()

	writeOrFatal(t, in_writer, "PLACE 0,0,NORTH\n")

	for _, expected := range []string{"EAST", "SOUTH", "WEST"} {
		writeOrFatal(t, in_writer, "RIGHT\n")
		if err := assertPosition(t, in_writer, out_reader, 0, 0, expected); err != nil {
			t.Fatalf("tried to rotate RIGHT: %s", err)
		}
	}
}

// writeOrFatal writes payload into w, or fails via t.Fatal
func writeOrFatal(t *testing.T, w io.Writer, payload string) {
	i, err := w.Write([]byte(payload))
	if err != nil {
		t.Fatal(err)
	}
	if i != len(payload) {
		t.Fatalf("expected to write %d bytes, only wrote %d", len(payload), i)
	}
}

// blockingReadLine waits for exactly one line followed by a \n from r
func blockingReadLine(r io.Reader) ([]byte, error) {
	var read []byte
	scanner := bufio.NewScanner(r)
	for {
		more := scanner.Scan()
		if !more {
			if err := scanner.Err(); err != nil {
				return nil, err
			}
			read = scanner.Bytes()
			if len(read) == 0 {
				// EOF, so we'll wait
				time.Sleep(100 * time.Millisecond)
				continue
			}
		} else {
			read = scanner.Bytes()
		}
		break
	}
	return read, nil
}

// assetPosition asserts that the current state a robot is at a given set of coordinates
// and optionally facing a given direction. to do this, it issues and parses a REPORT
func assertPosition(t *testing.T, input io.Writer, output io.Reader, x, y int, direction string) error {
	writeOrFatal(t, input, "REPORT\n")
	read, err := blockingReadLine(output)
	if err != nil {
		return fmt.Errorf("could not read output: %s", err)
	}
	reportOutput := reportMatcher.FindSubmatch(read)
	if len(reportOutput) == 0 {
		return fmt.Errorf("could not parse REPORT output")
	}

	// we need the first capture groups turned into integer coordinates
	var actualX, actualY int64
	if actualX, err = strconv.ParseInt(string(reportOutput[1]), 10, 64); err != nil {
		t.Fatal(err)
	}
	if actualY, err = strconv.ParseInt(string(reportOutput[2]), 10, 64); err != nil {
		t.Fatal(err)
	}

	// clean \n from direction output
	actualDirection := string(reportOutput[3])
	if idx := strings.Index(actualDirection, "\n"); idx >= 0 {
		actualDirection = actualDirection[0:idx]
	}

	if !(actualX == int64(x) && actualY == int64(y)) {
		return fmt.Errorf("expected (X:%d, Y:%d), got (X:%d, Y:%d)", x, y, actualX, actualY)
	}

	if direction != "" {
		if direction != string(actualDirection) {
			return fmt.Errorf("expected direction to be %s, got %s", direction, actualDirection)
		}
	}

	return nil
}
