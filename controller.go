package main

import (
	"bufio"
	"io"
	"log"
	"regexp"
)

// all possible inputs need a constant here to become its code.
const (
	Place  = iota
	Move   = iota
	Left   = iota
	Right  = iota
	Report = iota
)

// controller represents a control channel to a robot.
// it must speak the text protocol defined by the inputs matchers.
type controller struct {
	// source is anything that can communicate plaintext commands
	source io.Reader
	// inputs is a buffered channel where we emit inputInstances
	// i.e. we only allow one control to be scheduled at a time
	inputs chan inputInstance
}

// input is a single type of command
type input struct {
	// code matches the consts in this file, used to introspect later.
	code int
	// matcher provides a regular expression, that if matched, indicates a string IS this input.
	// it also emits [][]byte of arguments present in the command, if any.
	matcher *regexp.Regexp
}

// inputInstance is a single instance of an input
type inputInstance struct {
	// code matches the consts in this file.
	code int
	// args may optionally contain raw arguments that were produced by the input.matcher
	args [][]byte
}

// inputs contains an input struct for all possible inputs.
var inputs []input = []input{
	{Place, regexp.MustCompile(`^PLACE (-?\d+),(-?\d+),(\w+)$`)},
	{Move, regexp.MustCompile(`^MOVE$`)},
	{Left, regexp.MustCompile(`^LEFT$`)},
	{Right, regexp.MustCompile(`^RIGHT$`)},
	{Report, regexp.MustCompile(`^REPORT$`)},
}

func newController(reader io.Reader) *controller {
	c := &controller{
		source: reader,
		inputs: make(chan inputInstance),
	}

	go c.processInput()
	return c
}

// processInput listens to its controller input and yields inputInstances
// when it matches them.
// this continues until the controller input is closed, and should be called in a goroutine.
func (c *controller) processInput() {
	scanner := bufio.NewScanner(c.source)
READLOOP:
	for {
		more := scanner.Scan()

		// encounted unexpected end of input
		if err := scanner.Err(); !more && err != nil {
			log.Print(err)
			break READLOOP
		}

		line := scanner.Bytes()
		for _, input := range inputs {
			if matched := input.matcher.FindSubmatch(line); matched != nil {
				// matched line to an input, yield its control code & arguments
				c.inputs <- inputInstance{input.code, matched[1:len(matched)]}
				continue READLOOP
			}
		}

		// expected (i.e. EOF) break in input
		if !more {
			break READLOOP
		}
	}
	close(c.inputs)
}
